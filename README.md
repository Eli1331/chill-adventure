# A Chill Adventure #

### What inspired this? ###
* One of my close friends owns a shaved ice business that sells hot chocolate in the Winter, and shaved ice in the Summer(it's called The Chill Zone, located on the Bentonville Square).
* I decided to make a game exploring The Chill Zone out of season, so you are the Winter product trying to navigate it during Summer. This is why you throw hot chocolate at shaved ice and a fridge!
* This is a pretty wacky project, I know, but it did teach me a lot this Summer, and I hope you enjoy it!

### How do I set it up? ###
* In order to play the game, you need to download this repository onto your windows computer, and extract the files from Extract_Me_Here to the root folder of this repository.
* Then, you double click the A Chill Adventure Application to run.

### Why does this exist? ###

* I wanted to build on the principles I learned from the M.A.S.H. project, by making a 2D platformer. The majority of this game is my own assets, except for the manipulation of a water asset, the coins, and the moving platforms.
* I wrote all of the code for this project, but I did follow some tutorials for inspiration and looked to outside help a few times when I was stuck.

### How do I play? ###

* Start the game after using the functional settings menu.
* Use WASD to move, spacebar to jump, and mouseclick to throw hot chocolate that has marshmallows coming off of it.
* Try to avoid the fridge and the snow cones, or your health will go down!
* The only way to kill the enemies is by hitting them with the hot chocolate, and then they will melt.
* This game is rather difficult, and the first two levels are unique, with the third being a duplicate to make sure that more levels could be added.

### What now? ###

* Enjoy the game, let me know if there are any bugs or any ideas that you have for this repository!
* Know that a project like this took an entire summer with several projects under my belt, so don't think you can just download unity and make this in a month!
* This was a long but very educational project that I would recommend as a next step for anyone trying to challenge their skills with unity or programming.

### Who do I talk to? ###

* The owner of this Repository is Eli Arnold, and you can send me an email at eli.j.arnold.47@gmail.com